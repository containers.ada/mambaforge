FROM docker.io/bitnami/minideb:buster

LABEL name="mambaforge" \
    summary="mambaforge" \
    version="23.1.0" \
    maintainer="Ada Loveless <git@jooa.xyz>"

ENV LANG C.UTF-8
ENV SHELL /bin/bash

# Install dependencies
RUN set -eux; \
    install_packages curl ca-certificates

# Install mamba
ENV MAMBA_SOURCE "https://github.com/conda-forge/miniforge/releases/download/23.1.0-3/Mambaforge-23.1.0-3-Linux-x86_64.sh"
ENV MAMBA_SHA256SUM "7a6a07de6063245163a87972fb15be3a226045166eb7ee526344f82da1f3b694"
RUN set -eux; \
    curl -LOsSf "${MAMBA_SOURCE}"; \
    echo "${MAMBA_SHA256SUM}  Mambaforge-23.1.0-3-Linux-x86_64.sh" | sha256sum -c; \
    bash "Mambaforge-23.1.0-3-Linux-x86_64.sh" -p /opt/mambaforge -b; \
    rm "Mambaforge-23.1.0-3-Linux-x86_64.sh"; \
    /opt/mambaforge/bin/mamba init
ENV PATH /opt/mambaforge/bin:${PATH}

VOLUME ["/opt/mambaforge/envs"]

ENTRYPOINT ["mamba"]
